FROM centos:7

RUN yum install -y rpm-build rpmdevtools yum-utils epel-release
ADD *.spec /root/rpmbuild/SPECS/
WORKDIR /root/
RUN mkdir -p rpmbuild/SOURCES
RUN spectool -g -R rpmbuild/SPECS/*.spec
RUN yum-builddep -y rpmbuild/SPECS/*.spec
RUN rpmbuild -ba rpmbuild/SPECS/*.spec
