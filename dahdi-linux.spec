###################################################################
#
#  dahdi-linux.spec - used to generate dahdi-linux rpms
#  For more info: http://www.rpm.org/max-rpm/ch-rpm-basics.html
#
#  This spec file uses default directories:
#  /usr/src/redhat/SOURCES - orig source, patches, icons, etc.
#  /usr/src/redhat/SPECS - spec files
#  /usr/src/redhat/BUILD - sources unpacked & built here
#  /usr/src/redhat/RPMS - binary package files
#  /usr/src/redhat/SRPMS - source package files
#
###################################################################
#
#  Global Definitions
#
###################################################################
%define aversion 2.11.1
%define arelease 1
%define actversion %(echo %{aversion}|sed -e "s/-.*$//g")
%define subvers %(echo %{aversion}|awk "/-/"|sed -e "s/^.*-//"|awk '{print "0." $1 "."}')
%define actrelease %(echo %{subvers}%{arelease}|sed -e "s/-/_/g")

%define kvers %(rpm -q kernel-devel --qf "%%{version}-%%{release}.$(uname -m)")

###################################################################
#
#  The Preamble
#  information that is displayed when users request info
#
###################################################################
Summary: The DAHDI project
Name: dahdi-linux
Version: %{actversion}
Release: %{actrelease}
License: GPL
Source: https://downloads.asterisk.org/pub/telephony/dahdi-linux-complete/releases/dahdi-linux-complete-%{version}+%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-root
URL: http://www.asterisk.org/
Vendor: Digium, Inc.
Packager: Asterisk Development Team <asteriskteam@digium.com>
%{?el5:Requires: yum-kmod}
Requires: dahdi-firmware
Requires: kmod-dahdi-linux
BuildRequires: kernel kernel-devel
BuildRequires: make gcc
#
# Other tags not used
#
#Distribution:
#Icon:
#Conflicts:
#Serial:
#AutoReqProv:
#ExcludeArch:

%description
The open source DAHDI project

%package devel
Summary: DAHDI libraries and header files for development
Requires: %{name} = %{version}-%{release}

%description devel
The static libraries and header files needed for building additional plugins/modules

%package firmware
Summary: DAHDI firmware

%description firmware
The firmware files for DAHDI devices

%package tools
Summary: DAHDI Linux tools

%description tools
DAHDI Linux tools

%package perl
Summary: DAHDI PERL API

%description perl
DAHDI PERL API

%package kmod
Summary: DAHDI kernel modules

%description kmod
DAHDI kernel modules

%package tonezone
Summary: Zaptel tonezone library for Asterisk
Provides: libtonezone
Provides: libtonezone2
Provides: libtonezone-dahdi

%description tonezone
Zaptel tonezone library for Asterisk

%package tonezone-devel
Summary: Zaptel tonezone library development files
Provides: libtonezone-devel
Provides: libtonezone2-devel
Provides: libtonezone-dahdi-devel

%description tonezone-devel
Zaptel tonezone library development files

###################################################################
#
#  The Prep Section
#  If stuff needs to be done before building, this is the section
#  Use shell scripts to do stuff like uncompress and cd into source dir 
#  %setup macro - cleans old build trees, uncompress and extracts original source 
#
###################################################################
%prep
%setup -n dahdi-linux-complete-%{version}+%{version}

###################################################################
#
#  The Build Section
#  Use shell scripts and do stuff that makes the build happen, i.e. make
#
###################################################################
%build
echo %{aversion} > .version
%make_build KVERS=%kvers perllibdir=%{_datadir}/perl5 libdir=%{_libdir}

###################################################################
#
#  The Install Section
#  Use shell scripts and perform the install, like 'make install', 
#  but can also be shell commands, i.e. cp, mv, install, etc.. 
#
###################################################################
%install
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/udev/rules.d/
%make_install KVERS=%kvers perllibdir=%{_datadir}/perl5 libdir=%{_libdir}

###################################################################
#
#  Install and Uninstall 
#  This section can have scripts that are run either before/after
#  an install process, or before/after an uninstall process
#  %pre - executes prior to the installation of a package
#  %post - executes after the package is installed
#  %preun - executes prior to the uninstallation of a package
#  %postun - executes after the uninstallation of a package
#
###################################################################
%post
ldconfig

###################################################################
#
#  Verify
#
###################################################################
%verifyscript

###################################################################
#
#  Clean
#
###################################################################
%clean
cd $RPM_BUILD_DIR
%{__rm} -rf %{name}-%{version} 
%{__rm} -rf /var/log/%{name}-sources-%{version}-%{release}.make.err
%{__rm} -rf $RPM_BUILD_ROOT

###################################################################
#
#  File List
#
###################################################################
%files
%defattr(-, root, root)
%doc README ChangeLog
%doc linux/UPGRADE.txt linux/README linux/LICENSE linux/LICENSE.LGPL
%doc tools/UPGRADE.txt tools/README tools/LICENSE tools/LICENSE.LGPL
%config %{_sysconfdir}/udev/rules.d/
%{_sysconfdir}/dahdi
%{_datadir}/dahdi

%files devel
%defattr(-, root, root)
%{_includedir}/dahdi/dahdi_config.h
%{_includedir}/dahdi/fasthdlc.h
%{_includedir}/dahdi/kernel.h
%{_includedir}/dahdi/user.h
%{_includedir}/dahdi/wctdm_user.h

%files firmware
%defattr(-, root, root)
/lib/firmware

%files tools
%defattr(-, root, root)
%{_sysconfdir}/bash_completion.d/dahdi
%{_sbindir}
%{_mandir}

%files perl
%defattr(-, root, root)
%{_datadir}/perl5

%files kmod
%defattr(-, root, root)
/lib/modules

%files tonezone
%defattr(-, root, root)
%{_libdir}/libtonezone.so*

%files tonezone-devel
%defattr(-, root, root)
%{_libdir}/libtonezone.*a
%{_includedir}/dahdi/tonezone.h
